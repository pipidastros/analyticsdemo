﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
/**
 * Analytics Demo Scene Example Scripts
 */
public class DemoScene : MonoBehaviour{
    private AnalyticsSDK sdk = null;        // SDK Instance

    // Start is called before the first frame update
    void Start(){
        /* Setup SDK */
        sdk = AnalyticsSDK.Instance;
        if (!sdk.automaticStatupEvent){
            sdk.TrackLevelStart();
        }
    }

    public void OnNewLevelButtonPressed(){
        sdk.TrackNewLevelOpened();
    }

    public void OnWinningButton(){
        sdk.TrackLevelComplete();
    }

    public void OnLoosingButton(){
        sdk.TrackLevelFailture();
    }

    public void OnPaymentButton(){
        sdk.TrackPaymentSuccess("com.companyname.gamename.productane", 50);
    }
}
