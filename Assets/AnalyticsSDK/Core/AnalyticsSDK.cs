﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
/* Note: Use Deprecated WWW Methods */
#pragma warning disable CS0618
/**
 * Analytics SDK Class
 */
public class AnalyticsSDK : MonoBehaviour{
    /*
     * This is Singleton Params.
     * To get Instance of AnalyticsSDK, plsease user AnalyticsSDK.Instance();
     */
    [HideInInspector] public static AnalyticsSDK Instance { get { return instance; } }      // Get Instance of SDK
    [HideInInspector] public static AnalyticsSDK instance = null;                           // Analytics Instance

    [Header("Project Settings")]
    public string productVersion = "1.0";                                                   // Current Product Version
    [Header("SDK Settings")]
    [Tooltip("Is Debug Mode?")]public bool debugMode = false;                               // SDK Debug Mode
    [Tooltip("Please, set your API Server URL")]public string serverURL = "https://example.com/";               // API Server URL
    [Tooltip("Cooldown for Events Queue is sec")]public float cooldownBeforeSend = 10F;     // Cooldown for Events Queue in Seconds
    [Tooltip("Max Queue Elements per Request")]public int maxQueueElements = 10;            // Max Queue Elements per Request
    [Header("Events Settings")]
    public bool automaticStatupEvent = true;                                                // Automatic Startup Event
    public bool automaticCrashEvent = true;                                                 // Automatic Crash Event
    public bool automaticQuitEvent = true;                                                  // Automatic Quit Event

    /* Queue */
    private bool _isAutoStartupEventSended = false;                                         // Auto Startup Event Flag
    public AnalyticsQueue _eventsQueue = new AnalyticsQueue();                             // Events Queue Object
    private float _timeToSendQueue = 5F;                                                    // Time to Next send of Queue

    /* SDK Folders */
    private string _pathToTempQueue = "/temp_queue.events";                                 // Path to Temp Queue
    private string _pathToExceptionTrace = "/sdk_exception.log";                            // Path to SDK Exception Trace

    /* Protected Area for POST Response */
    protected string _lastErrorMessage;                                                     // Last Error Message
    protected long _lastErrorCode;                                                          // Last Error Code

    /* State Machine Events */
    public delegate void AnalyticsActionError(string message, long code);
    public event AnalyticsActionError OnAnalyticsError;

    #region Initialization
    /**
     * Awake Method. Used for SDK Initialization
     */
    private void Awake(){
        /* Check SDK Instance. It's a realisation of Singleton */
        if (instance == null){
            instance = this;
        }else if (instance == this){
            Destroy(gameObject);
        }

        /*
         * Cant destroy this object when load another
         * scene for singleton working state
         */
        DontDestroyOnLoad(gameObject); // Don't Destroy Gamebase

        /* Setup SDK Path */
        _pathToTempQueue = Application.persistentDataPath + _pathToTempQueue;
        _pathToExceptionTrace = Application.persistentDataPath + _pathToExceptionTrace;

        /* Load Temporary Queue */
        _loadTemporaryQueue();
    }

    /**
     * Application Start
     */
    private void Start(){
        /* Add Sending Event and Application Startup */
        if (automaticStatupEvent){
            TrackAppStart(); // Send Event
        }
    }

    /**
     * Update is called once per frame
     */
    private void Update(){
        /* Work With Queue */
        if(_timeToSendQueue <= 0F){
            _sendEventsQueue();
            _timeToSendQueue = cooldownBeforeSend;
        }else{
            _timeToSendQueue -= Time.deltaTime;
        }
    }

    /**
     * On Component Enabled
     */
    private void OnEnable(){
        Application.logMessageReceived += _handleExceptions;
    }

    /**
     * On Component Disabled
     */
    private void OnDisable(){
        Application.logMessageReceived -= _handleExceptions;
    }

    /**
     * Log Message
     */
    public void Log(string message){
        if (debugMode){
            Debug.Log(message);
        }
    }

    /**
     * Handle Application Exceptions and Write to File in Debug Mode
     * @param string logString Log Message
     * @param string stackTrace Stack Trace
     * @param LogType type Log Type
     */
    protected void _handleExceptions(string logString, string stackTrace, LogType type){
        if (type != LogType.Warning && type != LogType.Log && debugMode){
            bool critical = (type == LogType.Error || type == LogType.Assert) ? false : true; // Critical Error
            string message = logString + ". \n\n Stack Trace: " + stackTrace; // Log Message
            string code = "Unknown Error Type";
            if (type == LogType.Error)
                code = "Script Error";
            if (type == LogType.Assert)
                code = "Script Assert";
            if (type == LogType.Exception)
                code = "Script Exception";

            // Save Log
            string _error_data = "";
            _error_data += "Game Error Type: " + code + "\n\n";
            _error_data += "================================== \n\n";
            _error_data += message;
            _saveFile(_pathToExceptionTrace, _error_data, false);
        }
    }

    /**
     * Load Temporary Queue
     * @return AnalyticsQueue Object with Analytics Queue
     */
    protected void _loadTemporaryQueue(){
        /* Load Queue */
        _eventsQueue = new AnalyticsQueue();
        _eventsQueue.events = new List<AnalyticEvent>();
        string _queueData = _loadFile(_pathToTempQueue, false);
        if (_queueData.Length > 0) _eventsQueue = JsonUtility.FromJson<AnalyticsQueue>(_queueData);
    }

    /**
     * Save Temporary Queue to File
     */
    protected void _saveTemporaryQueue(){
        string _queueData = JsonUtility.ToJson(_eventsQueue);
        _saveFile(_pathToTempQueue, _queueData, false);
    }

    /**
     * Save File
     * @param string path Path to File
     * @param string data Data to Save
     * @param bool encoded Base64 Encoded file?
     */ 
    protected void _saveFile(string path, string data, bool encoded = false){
        if (encoded){
            byte[] _bytesToEncode = Encoding.UTF8.GetBytes(data); 
            data = Convert.ToBase64String(_bytesToEncode);
        }

        // Save File
        File.WriteAllText(path, data);
        Log("File saved to " + path);
    }

    /**
     * Load File
     * @param string path Path to File
     * @param bool encoded Decode Base64 Encoded file?
     * @return string File Content
     */
    protected string _loadFile(string path, bool encoded = false){
        if (!File.Exists(path)) {
            Log("Failed to load file. File " + path + " not found");
            return "";
        }

        /* Load File */
        string _data = File.ReadAllText(path);
        if (encoded){
            byte[] _decodedBytes = Convert.FromBase64String(_data);
            string _decodedData = Encoding.UTF8.GetString(_decodedBytes);
            return _decodedData;
        }

        // Return Data
        return _data;
    }
    #endregion

    #region Analytics Actions
    /**
     * On Application Startup Event
     */
    public void TrackAppStart(){
        /* Only Once for Start() */
        if (_isAutoStartupEventSended) return;
        _sendBaseEvent("game_startup");
    }

    /**
     * On Level Startup Event
     */
    public void TrackLevelStart(){
        _sendBaseEvent("level_startup");
    }

    /**
     * On New Level Opened
     */
    public void TrackNewLevelOpened(){
        _sendBaseEvent("new_level_opened");
    }

    /**
     * On Level Complete
     */
    public void TrackLevelComplete(){
        _sendBaseEvent("level_complete");
    }

    /**
     * On Level Failed
     */
    public void TrackLevelFailture(){
        _sendBaseEvent("level_failture");
    }

    /**
     * Payment Initialization Event
     */
    public void TrackPaymentInitialization(string productID, float summ, string currency = "USD"){
        _sendPaymentEvent("purchase_initialize", productID, summ, currency);
    }

    /**
     * Track Payment Success
     */
    public void TrackPaymentSuccess(string productID, float summ, string currency = "USD"){
        _sendPaymentEvent("purchase_complete", productID, summ, currency);
    }

    /**
     * Track Payment Error
     */
    public void TrackPaymentError(string productID, float summ, string currency = "USD"){
        _sendPaymentEvent("purchase_error", productID, summ, currency);
    }
	
	/**
	 * Fire Base Event
	 */
	public void FireBaseEvent(string type){
		_sendBaseEvent(type);
	}

    /**
     * Send Payment Event
     */
    protected void _sendPaymentEvent(string type, string productID, float summ, string currency = "USD"){
        /* Setup Event Data */
        AnalyticEvent eventData = new AnalyticEvent();
        eventData.type = type;
        eventData.data = new AnalyticEvent.AnalyticEventData();
        eventData.data.version = productVersion;
        eventData.data.time = (long)Time.deltaTime;
        eventData.data.level = SceneManager.GetActiveScene().buildIndex;
        eventData.data.platform = _getSDKPlatform();
        eventData.purchase = new AnalyticEvent.AnalyticPurcahseData();
        eventData.purchase.currency = currency;
        eventData.purchase.productID = productID;
        eventData.purchase.summ = summ;

        /* Send Event Data */
        _addEventToQueue(eventData);
    }

    /**
     * Send Base Event
     */
    protected void _sendBaseEvent(string type){
        /* Setup Event Data */
        AnalyticEvent eventData = new AnalyticEvent();
        eventData.type = type;
        eventData.data = new AnalyticEvent.AnalyticEventData();
        eventData.data.version = productVersion;
        eventData.data.time = (long)Time.deltaTime;
        eventData.data.level = SceneManager.GetActiveScene().buildIndex;
        eventData.data.platform = _getSDKPlatform();

        /* Send Event Data */
        _addEventToQueue(eventData);
    }

    /* TODO: Here we can add more methods for easly event fire */

    /**
     * Track Event
     * @param object trackEvent Event Data
     */
    public void TrackEvent(AnalyticEvent trackEvent){
        _addEventToQueue(trackEvent);
    }
    #endregion

    #region API
    /**
     * Get Latest Error Message
     * @return string Error Message
     */
    public string LatestErrorMessage(){
        return _lastErrorMessage;
    }

    /**
     * Get Latest Error Code
     */
    public long LatestErrorCode(){
        return _lastErrorCode;
    }

    /**
     * Add Event to Queue
     * @param object eventToTrack Event to add in Queue
     */
    protected void _addEventToQueue(AnalyticEvent eventToTrack){
        _eventsQueue.events.Add(eventToTrack);
        _saveTemporaryQueue();
    }

    /**
     * Remove From Queue
     * @param int index Event Index in Queue
     */
    protected void _removeEventFromQueue(int index){
        _eventsQueue.events.RemoveAt(index);
        _saveTemporaryQueue();
    }

    /**
     * Clear Events Queue
     */
    protected void _clearEventsQueue(){
        _eventsQueue.events.Clear();
        _saveTemporaryQueue();
    }

    /**
     * Send Events Queue
     */
    protected void _sendEventsQueue(){
        /* Check Queue Length */
        if(_eventsQueue.events.Count < 1){
            Log("No Events to send tracking data.");
        }

        /* Generate Limited Queue for Sending */
        int max = (_eventsQueue.events.Count < maxQueueElements) ? _eventsQueue.events.Count : maxQueueElements;
        AnalyticsQueue _temp = new AnalyticsQueue();
        _temp.events = new List<AnalyticEvent>();
        for (int i = 0; i < max; i++){
            _temp.events.Add(_eventsQueue.events[i]);
        }

        /* Send Events Queue */
        Log("Sending " + max.ToString() + " events from " + _eventsQueue.events.Count);
        _callAPIMethod(APIActions.TrackEvent, _temp, () =>{
            Log("All " + max.ToString() + " events sucessfully sended. Next Queue sending after " + cooldownBeforeSend + " seconds.");

            /* Save new Queue */
            _eventsQueue.events.RemoveRange(0, max);
            _saveTemporaryQueue();
        }, (error, code) =>{
            Log("Failed to send events Queue. Error: " + error + ". Retry after: " + cooldownBeforeSend + " seconds.");
        });
    }

    /**
     * Get SDK Platform
     * @return string Platform Name
     */
    protected string _getSDKPlatform(){
        /* Detect Application Platform */
        if (Application.platform == RuntimePlatform.Android){
            return "android";
        }else if (Application.platform == RuntimePlatform.IPhonePlayer){
            return "ios";
        }else if (Application.platform == RuntimePlatform.LinuxEditor || Application.platform == RuntimePlatform.LinuxPlayer ||
            Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer){
            return "pc";
        }else if (Application.platform == RuntimePlatform.WebGLPlayer){
            return "webgl";
        }else{
            return "other";
        }
    }

    /**
     * Send API Request
     * @param string method Method Name. For Example: APIActions.TrackEvent
     * @param object data Request Data Object
     * @param OnPOSTRequestSuccess OnSuccess Success Callback or Null
     * @param OnPOSTRequestError OnError Error Callback or Null
     */
    protected delegate void OnAPICallbackSuccess();
    protected delegate void OnAPICallbackError(string message, long code);
    protected void _callAPIMethod(string method, AnalyticsQueue data, OnAPICallbackSuccess OnSuccess = null, OnAPICallbackError OnError = null){
        string _final_url = serverURL + method;
        string _json_data = JsonUtility.ToJson(data);
        StartCoroutine(_sendPOSTRequest(_final_url, _json_data, () =>{
            if (OnSuccess != null) OnSuccess();
        }, (error, code) =>{
            _lastErrorMessage = error;
            _lastErrorCode = code;
            if (OnError != null) OnError(error, code);
            if (OnAnalyticsError != null) OnAnalyticsError(error, code);
        }));
    }

    /**
     * Send POST Request to URL using UnityWebRequest with Callback
     * @param string url POST Url
     * @param string data JSON Data
     * @param OnPOSTRequestSuccess OnSuccess Success Callback or Null
     * @param OnPOSTRequestError OnError Error Callback or Null
     */
    protected delegate void OnPOSTRequestSuccess();
    protected delegate void OnPOSTRequestError(string message, long code);
    protected IEnumerator _sendPOSTRequest(string url, string data, OnPOSTRequestSuccess OnSuccess, OnPOSTRequestError OnError){
        /* Build POST Request */
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(data);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        Log("Send Request to: " + url + " with data:" + data);
        yield return request.SendWebRequest();

        /* Work with Response */
        if (request.error != null){
            OnError(request.error, request.responseCode);
        }else if(request.responseCode == 200){
            OnSuccess();
        }else{
            OnError("Failed to send API Request. Error No: "+request.responseCode, request.responseCode);
        }
    }
    #endregion
}

