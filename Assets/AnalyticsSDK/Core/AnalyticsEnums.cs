﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
/**
 * Analytics API Actions Mapping
 */
public static class APIActions{
    public static string TrackEvent = "/event/track/";          // API Actions URL
}