﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
/**
 * Command Line Menu for SDK
 */
public class AnalyticsInitializer : EditorWindow{
    [MenuItem("Analytics SDK/Add to Scene")]
    static void initializeSDKObject(){
        GameObject _sdk = new GameObject("AnalyticsSDK");
        _sdk.transform.SetSiblingIndex(0);
        _sdk.AddComponent<AnalyticsSDK>();
        Selection.activeGameObject = _sdk;
    }

    [MenuItem("Analytics SDK/Show Documentation")]
    static void showDocs(){
        Application.OpenURL("https://gitlab.com/pipidastros/analyticsdemo");
    }

    [MenuItem("Analytics SDK/Show Dashboard")]
    static void showDashboard(){
        Application.OpenURL("https://example.com/dashboard/");
    }
}
