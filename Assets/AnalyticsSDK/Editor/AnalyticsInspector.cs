﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
/**
 * Custom SDK Inspector
 */
[CustomEditor(typeof(AnalyticsSDK))]
public class AnalyticsInspector : Editor{
    private static readonly string[] _dontIncludeMe = new string[] { "m_Script" };

    // Draw Inspector GUI
    public override void OnInspectorGUI(){
        // Draw Ocugine Buttons
        GUILayout.Space(10f);
        if (GUILayout.Button("Open Documentation", GUILayout.Width(273))){
            Application.OpenURL("https://gitlab.com/pipidastros/analyticsdemo");
        }

        // Draw Header
        GUILayout.Space(10f);
        GUILayout.Label("SDK Setup:", EditorStyles.boldLabel);

        // Draw Object Params
        serializedObject.Update();
        DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
        serializedObject.ApplyModifiedProperties();
    }
}
