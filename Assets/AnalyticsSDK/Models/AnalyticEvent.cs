﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************
/*  Analytics Demo Project
/*  With this library you can build your own Simple
/*  Analytics for Android, iOS, WebGL or PC Games.
/*
/*  @version                0.1.0
/*  @build                  100
/*  @developer              Ilya Rastorguev
/*  @url                    https://gitlab.com/pipidastros/analyticsdemo
/***************************************************/
#region Queue Model
/**
 * Queue Model
 */
[System.Serializable]
public class AnalyticsQueue{
    public List<AnalyticEvent> events;
}
#endregion

#region Analytic Event Model
/**
 * Analytics Event Base Model
 */
[System.Serializable]
public class AnalyticEvent{
    public string type;                         // Event Type
    public AnalyticEventData data;              // Event Data
    public AnalyticPurcahseData purchase;       // Purchase Data

    [System.Serializable]
    public class AnalyticEventData{
        /* Base Flags */
        public long time;                       // Event Time
        public string version;                  // Client Version
        public int level;                       // Current Level Index
        public string platform;                 // Platform
    }

    [System.Serializable]
    public class AnalyticPurcahseData{
        /* Purchase Flags */
        public string productID;                // Product ID
        public float summ;                      // Product Summ
        public string currency;                 // Product Currency
    }
}
#endregion