# Analytics Example
This example allows you to create your own Event-based analytics system. You can use it in your projects.

Developed by **Ilya Rastorguev**

## Installation
1) Download AnalyticsSDK <a href="https://gitlab.com/pipidastros/analyticsdemo/-/raw/Package/AnalyticsSDK.unitypackage">here</a>.
2) Add SDK to Unity ("Assets"=>"Import Package"=>"Custom Package") and select AnalyticsSDK.unitypackage
3) Initialize SDK using ("Analytics SDK"=>"Add to Scene")
4) Setup your SDK in the Inspector (we recommend to use "Debug mode" in development)
5) Done!

## Tracking Events
> **NOTE:** Don't forgot to add AnalyticsSDK component to the scene.

**To get access to the Analytics Module from any class you can use this method:**
```csharp
AnalyticsSDK SDK = AnalyticsSDK.Instance;
```

**Now you can Track your event:**
```csharp
// Send Basic Event with SDK
SDK.FireBaseEvent("my_event");
```

## Basic Events
**All basic events use this model:**
```csharp
/**
 * Analytics Event Base Model
 */
[System.Serializable]
public class AnalyticEvent{
    public string type;                         // Event Type
    public AnalyticEventData data;              // Event Data
    public AnalyticPurcahseData purchase;       // Purchase Data

    [System.Serializable]
    public class AnalyticEventData{
        /* Base Flags */
        public long time;                       // Event Time
        public string version;                  // Client Version
        public int level;                       // Current Level Index
        public string platform;                 // Platform
    }

    [System.Serializable]
    public class AnalyticPurcahseData{
        /* Purchase Flags */
        public string productID;                // Product ID
        public float summ;                      // Product Summ
        public string currency;                 // Product Currency
    }
}
```

## Purchase Event
**You can use purchase events:**
```csharp
SDK.TrackPaymentInitialization("my_product_id", 10F, "USD");    // Payment Initialization
SDK.TrackPaymentSuccess("my_product_id", 10F, "USD");           // Payment Success
SDK.TrackPaymentError("my_product_id", 10F, "USD");             // Payment Error
```

## Queue Support and Offline Mode
This library supports Queue event tracking with limits and offline storing.
You can setup your limit events per request and requests interval in the **SDK Inspector**.